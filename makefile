VULKAN_SDK := $(file <env/vulkan-sdk.txt)
VULKAN_RUNTIME := $(file <env/vulkan-runtime.txt)
COMPILE_ARGS := -O3 -I include -I "$(VULKAN_SDK)/include"
LINK_ARGS := -L lib -L "$(VULKAN_RUNTIME)" -l stdc++ -l vulkan-1 -l glfw3
BUILD := build
EXEC := $(shell basename "$(CURDIR)")$(if $(filter-out Windows_NT,$(OS)),,.exe)
OUTPUT_ARGS := -o $(BUILD)/$(EXEC)

.PHONY: build start setup clean

build:
	mkdir -p $(BUILD)
	gcc $(wildcard src/*.cpp) $(COMPILE_ARGS) $(LINK_ARGS) $(OUTPUT_ARGS)
	cp $(wildcard lib/*.dll) $(BUILD)

start:
	$(BUILD)/$(EXEC)

setup:
	git update-index --assume-unchanged $(wildcard env/*)

clean:
	rm -rf $(BUILD)
