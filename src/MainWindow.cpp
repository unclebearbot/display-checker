#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <iostream>
#include "MainWindow.h"

using namespace nvxarm;

void MainWindow::launch()
{
    init();
    run();
    destroy();
}

void MainWindow::init()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    window = glfwCreateWindow(640, 480, "Display Checker", nullptr, nullptr);
    uint32_t exts = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &exts, nullptr);
    std::cerr << "supported extensions: " << exts << std::endl;
    glm::mat4 matrix;
    glm::vec4 vector;
    auto test = matrix * vector;
}

void MainWindow::run()
{
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
    }
}

void MainWindow::destroy()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}
