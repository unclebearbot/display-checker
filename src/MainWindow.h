#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <iostream>

namespace nvxarm
{
    class MainWindow
    {
    public:
        void launch();

    private:
        GLFWwindow *window;
        void init();
        void run();
        void destroy();
    };
}
